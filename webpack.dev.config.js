const path = require('path'); // пакет, который генерит абсолютные пути, так как на относительные как ./dist он ругается
// const TerserPlugin = require('terser-webpack-plugin'); // минифицирует бандл
// const MiniCssExtractPlugin = require('mini-css-extract-plugin'); // собирает все стили в один файл // не нужно так как на деве нам не надо минифицировать бандл
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: {
		'hello-world': './src/hello-world.js',
		'kiwi': './src/kiwi.js',
	}, // в этот файл вебпак будет закидывать все, и начинать сборку с него
	output: {
		filename: '[name].bundle.js', // убираем hash так как в деве нам не нужен кеш
		path: path.resolve(__dirname, './dist'), // __dirname - current directory, './dist' указываем относительный желаемый путь
		publicPath: '/static/' // путь для того, чтобы вебпак знал где находятся все сгенеренные файлы // убрали потому что генерим html автоматом и он сам подцепляет префикс из path
	},
	mode: 'development', // none, dev or prod
	devServer: {
		contentBase: path.resolve(__dirname, './dist'), // где брать контент
		index: 'index.html', // имя и путь к indexhtml
		port: 9000, // порт
	},
	module: {
		rules: [ // простой массив с правилами, каждое правило это отдельный объект
			{
				test: /\.(png|jpg)$/, // регулярка на проверку расширений
				use: [ // определяем тут, какой лоадер должен быть использован для импорта изображений
					'file-loader' // loaders устанавливается каждый отдельно
				]
			},
			{
				test: /\.css$/,
				use: [
					'style-loader', 'css-loader' // возможно комбинировать несколько лоадеров вместе css-loader соберет наш css из файла и стайл лоудер создаст стилевые теги в html page
				]
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader', 'css-loader', 'sass-loader' // порядок важен, так как веббак читает лоадеры справа налево
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/, // exclude исключает файлы для лоадера
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/env'], // options поддерживаются для любого лоадера, preset env включает все самое новое в JS и даунит это до ES5
						plugins: ['transform-class-properties']
					}
				}
			},
			{
				test: /\.hbs$/,
				use: [
					'handlebars-loader'
				]
			}
		]
	},
	plugins: [
		// терсер тоже убрали так как минифицировать не надо
		// new MiniCssExtractPlugin({
		// 	filename: 'styles.[contenthash].css', // можем задавать имя итогового css файла
		// }), // не нужно так как на деве нам не надо минифицировать бандл
		new CleanWebpackPlugin({
			cleanOnceBeforeBuildPatterns: [ // указываем какие файлы где чистить, пути до файлов относительны output directory вебпака
				'**/*',
				path.join(process.cwd(), 'build/**/*') // удалит все файлы в build folder
			]
		}),
		new HtmlWebpackPlugin({ // можем кастомить выходной файл и другими опциями, менять метатеги, имя файла и т.д посмотреть можно на github плагина
			filename: 'hello-world.html',
			chunks: ['hello-world'], // chunk берется из entrypoint
			title: 'Hello world',
			template: 'src/page-template.hbs', // говорит webpack использовать наш темплейт для генерации html, можно юзать pug и т.д
			description: 'Some description',
		}),
		new HtmlWebpackPlugin({ // можем кастомить выходной файл и другими опциями, менять метатеги, имя файла и т.д посмотреть можно на github плагина
			filename: 'kiwi.html',
			chunks: ['kiwi'], // chunk берется из entrypoint
			title: 'Kiwi',
			template: 'src/page-template.hbs', // говорит webpack использовать наш темплейт для генерации html, можно юзать pug и т.д
			description: 'Kiwi desc',
		}),
	]
}