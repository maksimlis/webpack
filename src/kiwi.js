import Heading from './components/heading/heading.js';
import Kiwi from './components/kiwi-image/kiwi-image.js';


const heading = new Heading();
const kiwi = new Kiwi();

heading.render('kiwi');
kiwi.render();

