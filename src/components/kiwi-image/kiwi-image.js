import Kiwi from './image.png';
import './kiwi-image.scss';

class KiwiImage {
	render() {
		const img = document.createElement('img');
		img.src = Kiwi;
		img.alt = 'IMAGE KIWI';
		img.classList.add('kiwi-image');
		document.body.appendChild(img);
	}
}

export default KiwiImage