import './hello-world-button.scss';

class HelloWorldButton {
	buttonCssClass = 'hello-world-button';
	render() {
		const button = document.createElement('button');
		button.innerHTML = 'Hello world';
		button.classList.add(this.buttonCssClass);
		button.onclick = () => {
			const p = document.createElement('p');
			p.innerHTML = 'Hello world';
			p.classList.add('hello-world-text');
			document.body.appendChild(p);
		}
		document.body.appendChild(button);
	}

}

export default HelloWorldButton;