// для сервера файлик

const express = require('express');
const path = require('path');
const fs = require('fs');
const app = express(); // инстанс экспресса

app.get('/hello-world/', function (req, res) { // функция вызывается каждый раз когда express получает request по этому роуту
	// отдадим dummy контент в браузер
	// res - instance of response obj
	const pathToHtmlFile = path.resolve(__dirname, '../dist/hello-world.html');
	const contentFromHtmlFile = fs.readFileSync(pathToHtmlFile, 'utf-8'); // fs читает контент html файла и отдает строку с текстом
	res.send(contentFromHtmlFile);
});
app.get('/kiwi/', function (req, res) {
	const pathToHtmlFile = path.resolve(__dirname, '../dist/kiwi.html');
	const contentFromHtmlFile = fs.readFileSync(pathToHtmlFile, 'utf-8'); // fs читает контент html файла и отдает строку с текстом
	res.send(contentFromHtmlFile);
});


app.use('/static', express.static(path.resolve(__dirname, '../dist')));

app.listen(3000, function () { // вызовется когда апликуха стартует
	console.log('App is running on http://localhost:3000/');
});